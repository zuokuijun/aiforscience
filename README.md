# AIForScience

___

本仓库主要记录流体力学与机器学习相关知识

___

<p align="center">
    <img src="./images/cover.jpg" />
</p>



## 流体力学

* [常用术语总结](./docs/professional.md)
* [流体的连续性、黏性与可压缩性](./docs/aerodynamics1.md)
* [随体导数或者物质导数的定义](./docs/material_derivative.md)
* [连续性方程](./docs/continuous.md)
* [动量方程](./docs/momentum.md)
* [能量方程](./docs/energy.md)
* [流体力学控制方程总结](./docs/control.md)

## 书籍

* [推荐书籍](./docs/book.md)
